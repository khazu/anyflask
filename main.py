from flask import Flask, request, jsonify, make_response
from flask_restful import Resource, Api, reqparse
from init import app, api

class Test(Resource):
  test_data = reqparse.RequestParser()
  test_data.add_argument('test', required=True)

  def get(self):
    return {'status':'success'}
    
  def post(self):
    args = self.test_data.parse_args()
    return {'status':args.test}

api.add_resource(Test,'/')


