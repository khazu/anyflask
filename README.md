# Anyflask

# Run
Command to run:
docker-compose up -d --build

deployed in:
localhost:8080

# Versions
python: 3.10.2
flask: 2.1.1
flask-sqlalchemy: 2.5.1
flask-bcrypt: 1.0.0
flask-cors: 3.0.10
flask-restful: 0.3.9
flask-login: 0.6.0
flask-mail: 0.9.1
mysqlclient: 2.1.0
requests: 2.27.1
boto3: 1.21.32
kubernetes: 23.3.0
opencensus-ext-azure: 1.1.3
werkzeug: 2.0.3
waitress: 2.1.1
